package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"github.com/shawntoffel/godockertest/api/protobuf-spec/adding"
	"github.com/shawntoffel/godockertest/service"
	"google.golang.org/grpc"
)

func main() {
	s := grpc.NewServer()
	defer s.GracefulStop()

	adding.RegisterAddingServer(s, service.NewService())

	l, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatalf("listen: %v", err)
	}

	go func() {
		err = s.Serve(l)
		if err != nil {
			log.Fatalf("serve: %v", err)
		}
	}()

	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatal("dial: " + err.Error())
	}

	client := adding.NewAddingClient(conn)

	req := adding.AddRequest{
		A: 1,
		B: 2,
	}

	reply, err := client.Add(context.Background(), &req)
	if err != nil {
		log.Fatalf("%v", err)
	}

	fmt.Printf("%d + %d = %d\n", req.A, req.B, reply.Result)
}

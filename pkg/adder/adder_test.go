package adder

import (
	"testing"
)

func TestAdd(t *testing.T) {
	a := 1
	b := 2

	expected := 3

	got := Add(a, b)

	if got != expected {
		t.Errorf("expected %d, got %d", expected, got)
	}
}

package service

import (
	"context"

	"github.com/shawntoffel/godockertest/api/protobuf-spec/adding"
	"github.com/shawntoffel/godockertest/pkg/adder"
)

type service struct{}

func NewService() adding.AddingServer {
	return &service{}
}

func (s service) Add(ctx context.Context, req *adding.AddRequest) (*adding.AddReply, error) {
	result := adder.Add(int(req.A), int(req.B))

	reply := &adding.AddReply{
		Result: int32(result),
	}

	return reply, nil
}

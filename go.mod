module github.com/shawntoffel/godockertest

require (
	github.com/golang/protobuf v1.2.0
	golang.org/x/net v0.0.0-20180911220305-26e67e76b6c3
	golang.org/x/sys v0.0.0-20180909124046-d0be0721c37e // indirect
	google.golang.org/genproto v0.0.0-20180914223249-4b56f30a1fd9 // indirect
	google.golang.org/grpc v1.15.0
)

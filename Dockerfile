FROM golang:1.11 as build
WORKDIR /go/src/github.com/shawntoffel/godockertest
COPY Makefile go.mod go.sum ./
RUN make deps
ADD . .
RUN make build-linux

FROM scratch
COPY --from=build /go/src/github.com/shawntoffel/godockertest/bin/godockertest /bin/godockertest
CMD ["godockertest"]
